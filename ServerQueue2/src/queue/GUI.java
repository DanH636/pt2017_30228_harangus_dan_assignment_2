package queue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

public class GUI extends JFrame {
	
	private JTextField f1,f2,f3,f4,f5,f6,f7;
	private JButton b1;
	private ArrayList<JLabel> l;
	private ArrayList<JTextField> t;
	private JTextArea a;
	private JScrollPane scroll;
	
	public GUI(){
		initUI();
	}
	
	private void initUI(){
		setTitle("Cozi");
		setSize(700,900);
		
		l=new ArrayList<JLabel>();
		t=new ArrayList<JTextField>();
		a=new JTextArea(30,30);
		scroll=new JScrollPane(a);
		
		setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
				
		add(new JLabel("Min arrival"));
		f1=new JTextField(10);
		add(f1);
		
		add(new JLabel("Max arrival"));
		f2=new JTextField(10);
		add(f2);
		
		add(new JLabel("Min service"));
		f3=new JTextField(10);
		add(f3);
		
		add(new JLabel("Max service"));
		f4=new JTextField(10);
		add(f4);
		
		add(new JLabel("Nr of queues"));
		f5=new JTextField(10);
		add(f5);
		
		add(new JLabel("Min simulation"));
		f6=new JTextField(10);
		add(f6);
		
		add(new JLabel("Max simulation"));
		f7=new JTextField(10);
		add(f7);
		
		b1=new JButton("Generate");
		add(b1);
		b1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int nrQueue=Integer.parseInt(f5.getText());
				for(JLabel p:l){
					remove(p);
				}
				l.clear();
				
				for(JTextField p:t){
					remove(p);
					}
				t.clear();
				remove(scroll);
				a.setText("");
				
				for(int i=0;i<nrQueue;i++){
					JLabel p=new JLabel("Casa "+i);
					l.add(p);
					JTextField x=new JTextField(30);
					t.add(x);
					add(p);
					add(x);
				}
				add(scroll);
				pack();
				
				 int i;
				 int nr=getNrQueues();
				 Casa c[] = new Casa[ nr];
				 for( i=0; i<nr; i++){
				 c[ i ] = new Casa("Casa "+i,getMinSim(),getMaxSim(),a,t.get(i) );
				 c[ i ].start();
				 } 
				 Simulare s = new Simulare( nr , c, "Simulare",getMinArrival(),getMaxArrival(),getMinService(),getMaxService(),getMinSim(),getMaxSim(),a);
				 s.start();
				 
			}
		});
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
	}
	
	public int getMinArrival(){
		return Integer.parseInt(f1.getText());
	}
	
	public int getMaxArrival(){
		return Integer.parseInt(f2.getText());
	}
	
	public int getMinService(){
		return Integer.parseInt(f3.getText());
	}
	
	public int getMaxService(){
		return Integer.parseInt(f4.getText());
	}
	
	public int getNrQueues(){
		return Integer.parseInt(f5.getText());
	}
	
	public int getMinSim(){
		return Integer.parseInt(f6.getText());
	}
	
	public int getMaxSim(){
		return Integer.parseInt(f7.getText());
	}
	
	public static void main(String[] args){
			GUI g=new GUI();
			g.setVisible(true);
			
			 }
}
	
	

