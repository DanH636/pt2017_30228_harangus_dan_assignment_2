package queue;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.JTextArea;

public class Simulare extends Thread{

	 private Casa casa[];
	 private ArrayList<Client> clienti;
	 private int nrCase;
	 static int ID =0;
	 private JTextArea a;

	 
	 private int minTS,maxTS,minTP,maxTP,minS,maxS;
	 
	 
	 public Simulare( int nr_case, Casa casa[], String name, int minTS, int maxTS, int minTP, int maxTP, int minS, int maxS,JTextArea a ){
		 
		 setName( name );
		 this.nrCase = nr_case;
		 this.casa = new Casa[ nr_case ];
		 this.minTS=minTS;
		 this.maxTS=maxTS;
		 this.minTP=minTP;
		 this.maxTP=maxTP;
		 this.minS=minS;
		 this.maxS=maxS;
		 this.a=a;
		 
		 for( int i=0; i<nr_case; i++){
			 this.casa[ i ] =casa[ i ];
		 	}
	 }
	 private int min_index (){
		 int index = 0;
		 try
		 {
			 long min = casa[0].lungCoada();
			 for( int i=1; i<nrCase; i++){
				 long lung = casa[ i ].lungCoada();
				 if ( lung < min ){
					 min = lung;
					 index = i;
				 }
			 }
		 }
		 catch( InterruptedException e ){
			 System.out.println( e.toString());
		 }
		 return index;
	 }
		
	 public ArrayList<Client> generateClienti(int minTS, int maxTS, int minTP, int maxTP,int minS,int maxS){
			Random r=new Random();
			int id=1;
			ArrayList<Client> a=new ArrayList<Client>();
			
			int timpSosire=minS-minTS;
			int timpServire=0;
			
			while(timpSosire<maxS){
				timpSosire+=r.nextInt(maxTS-minTS)+minTS;
				timpServire=r.nextInt(maxTP-minTP)+minTP;
				a.add(new Client(id,timpSosire,timpServire));
				id++;
			}
			return a;

		}

	 public void run(){
		 try
		 {
			 clienti=generateClienti(minTS,maxTS,minTP,maxTP,minS,maxS);
			 
			 int avgWaitingTime=0;
			 int avgServiceTime=0;
			 clienti.size();
			 for(int i=minS;i<=maxS;i++){
				 if(clienti.size()>0){
					 if(clienti.get(0).getTsosire()==i){
						Client c=clienti.remove(0);
						 int m = min_index();
						 a.append("Client :" +c.getID()+" adaugat la Casa "+Integer.toString(m)+" la timpul "+c.getTsosire()+"\n");
						 casa[ m ].adaugaClient( c );
						 
						 
					 }
				 }
				 sleep(1000);

				 
			 }
			 sleep(10000);
			 for(Casa p:casa){
				 avgWaitingTime+=p.getWaitingTime();
				 avgServiceTime+=p.getServiceTime();
			 }
			 avgWaitingTime=avgWaitingTime/casa.length;
			 avgServiceTime=avgServiceTime/casa.length;
			 a.append("Timpul mediu de asteptare:" + avgWaitingTime+"\n");
			 a.append("Timpul mediu de servire:"+ avgServiceTime+"\n");
			 
			 
		 }
		 catch( InterruptedException e ){
			 System.out.println( e.toString());
		 }
	 	}
} 
