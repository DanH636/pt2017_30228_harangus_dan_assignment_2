package queue;

import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Casa extends Thread {
	private Vector<Client> clienti=new Vector<Client>();
	private int simStart,simFinish;
	private int waitingTime=0;
	private int serviceTime=0;
	private JTextArea a;
	private JTextField t;
	
	public Casa(String name, int simStart, int simFinish,JTextArea a,JTextField t){
		setName(name);
		this.simStart=simStart;
		this.simFinish=simFinish;
		this.a=a;
		this.t=t;
	}
	
	public void run(){
		Client c;
		try{
			while(true){
				
				c=getClient();
				t.setText("");
				for(Client p:clienti){
					t.setText(t.getText()+" "+p.getID());
				}
				
				if(simStart>c.getTsosire())
				{
				waitingTime+=simStart-c.getTsosire();
				
				
				
				}
				else{
					simStart=c.getTsosire();
				}
				a.append("Clientul "+c.getID()+" este servit la timpul "+simStart+"\n");
				serviceTime+=c.getTservire();
				sleep(2000*serviceTime);
				simStart+=c.getTservire();
				clienti.remove(0);
				a.append("Clientul "+c.getID()+" a parasit casa la timpul "+simStart+"\n");
			}
		}
		catch(InterruptedException e){
			System.out.println("Intrerupere");
			System.out.println(e.toString());
		}
		
	}
	
	public synchronized void adaugaClient(Client c) throws InterruptedException{
		clienti.addElement(c);
		notifyAll();
	}
	
	public synchronized Client getClient() throws InterruptedException{
		while(clienti.size()==0)
			wait();
		Client c1=(Client)clienti.elementAt(0);
		notifyAll();
		return c1;
	}
	
	public Vector<Client> getListClienti(){
		return clienti;
	}
	

	
	public synchronized long lungCoada() throws InterruptedException{
		notifyAll();
		long size=clienti.size();
		return size;
	}
	
	public int getServiceTime(){
		return serviceTime;
	}
	
	public int getWaitingTime(){
		return waitingTime;
	}
}
