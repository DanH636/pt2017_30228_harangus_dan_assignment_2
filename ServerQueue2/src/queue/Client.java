package queue;



public class Client{
	private int id;
	private int timpSosire;
	private int timpServire;

	
	
	public Client(int id,int timpSosire,int timpServire){

		this.id=id;
		this.timpSosire=timpSosire;
		this.timpServire=timpServire;
		
	}
	
	public int getID(){
		return id;
	}
	
	public int getTsosire(){
		return timpSosire;
	}
	
	public int getTservire(){
		return timpServire;
	}
	

	
	public String toString(){
		return(Integer.toString(id)+" "+timpSosire+" "+timpServire);
	}


}
